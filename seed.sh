#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username postgres --dbname "$POSTGRES_DB" <<-EOSQL
  CREATE DATABASE calendar;
  CREATE ROLE calendar WITH LOGIN PASSWORD 'calendar';
  GRANT ALL PRIVILEGES ON DATABASE calendar TO calendar;
EOSQL
