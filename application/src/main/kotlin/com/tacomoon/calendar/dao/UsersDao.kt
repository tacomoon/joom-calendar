package com.tacomoon.calendar.dao

import com.tacomoon.calendar.domain.User
import com.tacomoon.calendar.util.DaoUtils
import com.tacomoon.calendar.util.getUUID
import java.util.UUID
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class UsersDao(private val namedParameterJdbcTemplate: NamedParameterJdbcTemplate) {

    companion object {
        // language=sql
        const val SELECT_FROM_USER = """
            SELECT u.user_id,
                   u.login,
                   u.name
            FROM users u
        """
    }

    fun findUser(id: UUID): User? {
        val sql = "$SELECT_FROM_USER WHERE u.user_id = :id"
        val params = mapOf("id" to id)

        return DaoUtils.nullableSingleResult(
            namedParameterJdbcTemplate.query(sql, params, userRowMapper)
        )
    }

    fun saveUser(user: User) {
        val sql = """
            INSERT INTO users (user_id, login, name) 
            VALUES (:id, :login, :name) 
        """
        val params = mapOf<String, Any>(
            "id" to user.id,
            "login" to user.login,
            "name" to user.name,
        )

        namedParameterJdbcTemplate.update(sql, params)
    }

    private val userRowMapper = RowMapper { rs, _ ->
        User(
            id = rs.getUUID("user_id"),
            login = rs.getString("login"),
            name = rs.getString("name"),
        )
    }
}