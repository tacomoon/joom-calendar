package com.tacomoon.calendar.dao

import com.tacomoon.calendar.domain.Invite
import com.tacomoon.calendar.util.getEnum
import com.tacomoon.calendar.util.getUUID
import java.util.UUID
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class InvitesDao(private val namedParameterJdbcTemplate: NamedParameterJdbcTemplate) {

    companion object {
        // language=sql
        const val SELECT_FROM_INVITES = """
            SELECT i.invite_id,
                   i.meeting_id,
                   i.organizer_id,
                   i.guest_id,
                   i.reply
            FROM invites i
        """
    }

    fun listInvitesByGuestId(guestId: UUID): List<Invite> {
        val sql = """
            $SELECT_FROM_INVITES
            WHERE i.guest_id = :guest_id
        """
        val params = mapOf("guest_id" to guestId)

        return namedParameterJdbcTemplate.query(sql, params, inviteRowMapper)
    }

    fun saveInvites(invites: List<Invite>) {
        val sql = """
            INSERT INTO invites (invite_id, meeting_id, organizer_id, guest_id, reply) 
            VALUES (:invite_id, :meeting_id, :organizer_id, :guest_id, :reply) 
        """

        val params = invites
            .map { invite -> invite.toParams() }
            .toTypedArray()

        namedParameterJdbcTemplate.batchUpdate(sql, params)
    }

    private fun Invite.toParams(): Map<String, Any> {
        return mapOf(
            "invite_id" to inviteId,
            "meeting_id" to meetingId,
            "organizer_id" to organizerId,
            "guest_id" to guestId,
            "reply" to reply,
        )
    }

    private val inviteRowMapper = RowMapper { rs, _ ->
        Invite(
            inviteId = rs.getUUID("invite_id"),
            meetingId = rs.getUUID("meeting_id"),
            organizerId = rs.getUUID("organizer_id"),
            guestId = rs.getUUID("guest_id"),
            reply = rs.getEnum("reply"),
        )
    }
}