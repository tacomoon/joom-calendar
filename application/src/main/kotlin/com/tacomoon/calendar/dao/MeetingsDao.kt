package com.tacomoon.calendar.dao

import com.tacomoon.calendar.domain.Meeting
import com.tacomoon.calendar.util.SQLArrayValue
import com.tacomoon.calendar.util.getEnum
import com.tacomoon.calendar.util.getInstant
import com.tacomoon.calendar.util.getList
import com.tacomoon.calendar.util.getUUID
import java.util.UUID
import org.springframework.jdbc.core.RowMapper
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.stereotype.Repository

@Repository
class MeetingsDao(private val namedParameterJdbcTemplate: NamedParameterJdbcTemplate) {

    companion object {
        // language=sql
        const val SELECT_FROM_MEETING = """
            SELECT m.meeting_id,
                   m.name,
                   m.host_id,
                   m.guest_id_list,
                   m.timestamp,
                   m.repeat
            FROM meetings m
        """
    }

    fun listMeetingsByHost(hostId: UUID): List<Meeting> {
        val sql = """
            $SELECT_FROM_MEETING
            WHERE m.host_id = :host_id
        """
        val params = mapOf("host_id" to hostId)

        return namedParameterJdbcTemplate.query(sql, params, meetingRowMapper)
    }

    fun saveMeeting(meeting: Meeting) {
        val sql = """
            INSERT INTO meetings (meeting_id, name, host_id, guest_id_list, timestamp, repeat) 
            VALUES (:meeting_id, :name, :host_id, :guest_id_list, :timestamp, :repeat) 
        """
        val params = mapOf(
            "meeting_id" to meeting.meetingId,
            "name" to meeting.name,
            "host_id" to meeting.hostId,
            "guest_id_list" to SQLArrayValue("UUID", meeting.guestIdList),
            "timestamp" to meeting.timestamp,
            "repeat" to meeting.repeat,
        )

        namedParameterJdbcTemplate.update(sql, params)
    }

    private val meetingRowMapper = RowMapper { rs, _ ->
        Meeting(
            meetingId = rs.getUUID("meeting_id"),
            name = rs.getString("name"),
            hostId = rs.getUUID("host_id"),
            guestIdList = rs.getList("guest_id_list"),
            timestamp = rs.getInstant("timestamp"),
            repeat = rs.getEnum("repeat"),
        )
    }
}