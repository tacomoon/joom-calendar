package com.tacomoon.calendar.domain

import java.time.Instant
import java.util.UUID

enum class RepeatType {
    NONE,
}

data class Meeting(
    val meetingId: UUID,
    val name: String,
    val hostId: UUID,
    val guestIdList: List<UUID>,
    val timestamp: Instant,
    val repeat: RepeatType,
)
