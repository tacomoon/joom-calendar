package com.tacomoon.calendar.domain

import java.util.UUID

data class User(
    val id: UUID,
    val login: String,
    val name: String,
)
