package com.tacomoon.calendar.domain

import java.util.UUID

enum class ReplyType {
    YES, NO, MAYBE
}

data class Invite(
    val inviteId: UUID,
    val meetingId: UUID,
    val organizerId: UUID,
    val guestId: UUID,
    val reply: ReplyType,
)
