package com.tacomoon.calendar

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Application

// TODO[EG]: replace logs with log4j?
// TODO[EG]: run sql migrations before application
fun main(args: Array<String>) {
    runApplication<Application>(*args)
}