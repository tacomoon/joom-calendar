package com.tacomoon.calendar.util

import java.sql.PreparedStatement
import java.sql.ResultSet
import java.time.Instant
import java.util.UUID
import org.springframework.jdbc.support.SqlValue

fun ResultSet.getUUID(columnLabel: String): UUID {
    return getObject(columnLabel, UUID::class.java)
}

fun ResultSet.getInstant(columnLabel: String): Instant {
    return getTimestamp(columnLabel).toInstant()
}

@Suppress("UNCHECKED_CAST")
fun <T> ResultSet.getList(columnLabel: String): List<T> {
    return (getArray(columnLabel).array as Array<T>).toList()
}

inline fun <reified E : Enum<E>> ResultSet.getEnum(columnLabel: String): E {
    return java.lang.Enum.valueOf(E::class.java, getString(columnLabel))
}

class SQLArrayValue(
    private val type: String,
    private val list: Collection<Any?>,
) : SqlValue {
    override fun setValue(ps: PreparedStatement, paramIndex: Int) {
        val value = ps.connection.createArrayOf(type, list.toTypedArray())
        ps.setArray(paramIndex, value)
    }

    override fun cleanup() {}
}

object DaoUtils {
    fun <T> nullableSingleResult(result: List<T>?): T? {
        if (result == null || result.isEmpty() || result.size > 1) {
            return null
        }

        return result[0]
    }
}