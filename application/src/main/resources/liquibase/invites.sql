--liquibase formatted sql

CREATE TYPE REPLY_TYPE AS ENUM ('YES', 'NO', 'MAYBE');

CREATE TABLE invites
(
    invite_id    UUID       NOT NULL,
    meeting_id   UUID       NOT NULL REFERENCES meetings (meeting_id),
    organizer_id UUID       NOT NULL REFERENCES users (user_id),
    guest_id     UUID       NOT NULL REFERENCES users (user_id),
    reply        REPLY_TYPE NOT NULL
);

CREATE INDEX invites__meeting__idx ON invites (meeting_id);

CREATE INDEX invites__guest_id__idx ON invites(guest_id);