--liquibase formatted sql

CREATE TYPE REPEAT_TYPE AS ENUM ('NONE');

CREATE TABLE meetings
(
    meeting_id    UUID PRIMARY KEY,
    name          TEXT        NOT NULL,
    host_id       UUID        NOT NULL REFERENCES users (user_id),
    guest_id_list UUID[]      NOT NULL,
    timestamp     TIMESTAMP   NOT NULL,
    repeat        REPEAT_TYPE NOT NULL
);

CREATE INDEX meetings__host_id__idx ON meetings (host_id);