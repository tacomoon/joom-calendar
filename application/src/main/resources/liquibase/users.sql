--liquibase formatted sql

CREATE TABLE users
(
    user_id UUID PRIMARY KEY,
    login   TEXT NOT NULL UNIQUE,
    name    TEXT NOT NULL
);